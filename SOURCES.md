# Sources

## Logiciels
### Leaflet
* Site Web : https://leafletjs.com/
* Dépôt : https://github.com/Leaflet/Leaflet
* Licence : BSD-2-Clause (https://raw.githubusercontent.com/Leaflet/Leaflet/master/LICENSE)
* Crédit : CloudMade, Vladimir Agafonkin et d'[autres personnes](https://github.com/Leaflet/Leaflet/graphs/contributors)
* Version : [Leaflet 1.4.0](https://github.com/Leaflet/Leaflet/releases/tag/v1.4.0) (version stable, publiée le 30/12/2018)

### IGCWebview2
* Dépôt : https://github.com/GlidingWeb/IgcWebview2/
* Licence : MIT Licence
* Crédit : Alistair Malcolm Green, Richard Paul Brisbourne et d'[autres personnes](https://github.com/GlidingWeb/IgcWebview2/graphs/contributors)
* Version : commit [05406b3](https://github.com/GlidingWeb/IgcWebview2/tree/05406b3905403731a0720f7a2949171435a83c61) (version publiée le 09/09/2017)
* Historique : https://github.com/alistairmgreen/jsigc / https://alistairmgreen.github.io/jsigc/
* Fichiers :
  * utilities.js → src/utilities.js
  * igc.js → src/igc.js

### toGeoJSON
* Dépôt : https://github.com/tmcw/togeojson
* Licence : BSD 2-Clause (https://raw.githubusercontent.com/tmcw/togeojson/master/LICENSE)
* Crédit : Mapbox, Tom et d'[autres personnes](https://github.com/tmcw/togeojson/graphs/contributors)
* Version : [0.16.0](https://github.com/tmcw/togeojson/releases/tag/v0.16.0) (publiée le 18/10/2016)
* Fichier : togeojson.js → ./togeojson-0.16.0/togeojson.js

### Leaflet.FileLayer
* Dépôt : https://github.com/makinacorpus/Leaflet.FileLayer
* Licence : MIT License
* Crédit : [Makina Corpus](https://github.com/makinacorpus), [plusieurs contributeurs](https://github.com/makinacorpus/Leaflet.FileLayer/graphs/contributors)
* Version : [1.2.0](https://github.com/makinacorpus/Leaflet.FileLayer/releases/tag/1.2.0) (publiée le 18/11/2017)
* Fichiers :
  * leaflet.filelayer.js → ./Leaflet.FileLayer-1.2.0/src/leaflet.filelayer.js
  * folder.svg → ./Leaflet.FileLayer-1.2.0/docs/folder.svg

### Plotly.js
* Site Web : https://plot.ly/javascript/
* Dépôt : https://github.com/plotly/plotly.js
* Licence : MIT Licence
* Crédit : Plotly, Inc. et [les contributeurs](https://github.com/plotly/plotly.js/graphs/contributors)
* Version : [1.45.3](https://github.com/plotly/plotly.js/releases/tag/v1.45.3) (publiée le 19/03/2019)
* Fichier : plotly-basic.min.js → ./plotly.js-1.45.3/dist/plotly-basic.min.js

## leaflet-ajax
* Dépôt : https://github.com/calvinmetcalf/leaflet-ajax
* Licence : MIT Licence
* Crédit : Calvin Metcalf et d'[autres contributeurs](https://github.com/calvinmetcalf/leaflet-ajax/graphs/contributors)
* Version : [2.1.0](https://github.com/calvinmetcalf/leaflet-ajax/releases/tag/v2.1.0) (publiée le 13/10/2016)
* Fichier : leaflet.ajax.min.js → ./leaflet-ajax-2.1.0/dist/leaflet.ajax.min.js

## leaflet-ruler
* Dépôt : https://github.com/gokertanrisever/leaflet-ruler
* Licence : MIT Licence
* Crédit : Goker Tanrisever et d'[autres contributeurs](https://github.com/gokertanrisever/leaflet-ruler/graphs/contributors)
* Version : commit [a84836f](https://github.com/gokertanrisever/leaflet-ruler/tree/a84836fd7665f258017fa84c27b43f5e88097554) (version publiée le 30/08/2018)
* Fichiers :
  * icon-colored.png → ./dist/icon-colored.png
  * icon.png → ./dist/icon.png
  * leaflet-ruler.css → ./src/leaflet-ruler.css
  * leaflet-ruler.js → ./src/leaflet-ruler.js

## Leaflet.RotatedMarker
* Dépôt : https://github.com/bbecquet/Leaflet.RotatedMarker
* Licence : MIT Licence
* Crédit : bbecquet et d'[autres contributeurs](https://github.com/bbecquet/Leaflet.RotatedMarker/graphs/contributors)
* Version : commit [6a3e9fe](https://github.com/bbecquet/Leaflet.RotatedMarker/tree/6a3e9fecfd0cfc76205484987f97975de6de3dcd) (version publiée le 21/05/2018)
* Fichier : leaflet.rotatedMarker.js → leaflet.rotatedMarker.js

## Cartes
### Liens vers les fonds de carte
* uMap ( https://umap.openstreetmap.fr/ )
  * OSM-Fr
  * OpenStreetMap
  * OSM Humanitarian (OSM-FR)
  * OSM Roads (uni-heidelberg)
  * OSM Landscape (Thunderforest)
  * OSM Transport (Thunderforest)
  * OSM Watercolor (Stamen)
* Géoportail ( https://www.geoportail.gouv.fr/donnees/carte-oaci-vfr )
  * Cartes « OACI-VFR » (IGN/SIA)
  * Images aériennes (IGN)

### Calques supplémentaires
* OpenAIP ( http://openaip.net/ )
  * Espaces aériens
  * Aérodromes

